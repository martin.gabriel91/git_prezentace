![gitlab](img/Git.png)

# GitLab školení

Podklady ke školení git / GitLab. Zdrojová prezentace byla vytvořena v Keynote, ale je vyexportována v PDF a PowerPointu, takže je dále volně editovatelná.



## Ukázkové projekty ze školení:

[Knihovna matematických funkcí](https://gitlab.com/martin.gabriel91/mathtest)

[Implementace kalkulačky s použitím knihovny jako submodulu](https://gitlab.com/martin.gabriel91/calculatortest)



## Software

#### Správa git repozitářů:

- [GitKraken](https://www.gitkraken.com/) - multiplatformní GUI klient
- [TortoiseGit](https://tortoisegit.org/) - Windows GUI klient
- [GitGui](https://git-scm.com/downloads) - multiplatformní GUI klient od tvůrců git (moc nedoporučuju)

#### Editace markdown dokumentů (Wiki)

- [Typora](https://typora.io/) - multiplatformní markdown editor (umí kopírovat sloupce z excelu, exportovat do PDF, live rendering, mermaid editaci grafů, spoustu dalších funkcí)
- [ghostwriter](https://wereturtle.github.io/ghostwriter/) - open-source multiplatformní markdown editor


## Užitečné odkazy:

[Oficiální git dokumentace](https://git-scm.com/docs)

[Oficiální GitLab dokumentace](https://about.gitlab.com/why/)

[Git Commit Best Practices](https://github.com/trein/dev-best-practices/wiki/Git-Commit-Best-Practices) - lady jak správně commitovat

[GitLab workflow](https://about.gitlab.com/2016/10/25/gitlab-workflow-an-overview/#gitlab-workflow) - shrnutí funkcí a využítí GitLabu

[Jak na git](http://www.kutac.cz/blog/pocitace-a-internety/jak-na-git-dil-0-co-proc-jak/) - česká série článků, kde autor pěkně a jednoduše vysvětluje git

[Git na ITnetwork.cz](https://www.itnetwork.cz/software/git) - další česká série článků

[ohsgitgit.com](http://ohshitgit.com/) - příklady častých chyb s řešením



Hodně zdarů s gitem! :)

![nobody](img/nobody-knows-git.jpg)

